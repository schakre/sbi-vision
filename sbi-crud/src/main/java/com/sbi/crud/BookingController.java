package com.sbi.crud;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.sbi.crud.model.Booking;
import com.sbi.crud.service.BookingLocalServiceUtil;

import java.util.Calendar;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Portlet implementation class Booking
 */

@Controller
@RequestMapping("VIEW")
public class BookingController {

	private static Log _log = LogFactoryUtil.getLog(BookingController.class);

	@RenderMapping
	public String handelDefaultRenderMapping(RenderRequest request,
			RenderResponse response) {

		return "view";
	}

	@RenderMapping(params = "action=addBooking")
	public String handelAddBookingRenderMapping(RenderRequest request,
			RenderResponse response) {

		return "saveBooking";
	}

	@RenderMapping(params = "action=formAddBooking")
	public String formAddBooking(RenderRequest request, RenderResponse response) {
		return "formAddBooking";
	}

	@ModelAttribute("booking")
	public Booking getBooking() throws Exception {
		Booking booking = null;
		if (Validator.isNull(booking)) {
			booking = BookingLocalServiceUtil
					.createBooking(CounterLocalServiceUtil.increment());
		}
		return booking;
	}

	@ActionMapping(params = "action=formAddBooking")
	public void getCustomerData(@ModelAttribute("booking") Booking booking,
			ActionRequest actionRequest, ActionResponse actionResponse,
			Model model) {
		_log.info("#############Calling getCustomerData##########");
		actionResponse.setRenderParameter("action", "success");
		model.addAttribute("successModel", booking);
	}

	@RenderMapping(params = "action=success")
	public String viewSuccess() {

		_log.info("#############Calling viewSuccess###########");
		/**
		 * Display success.jsp
		 */
		return "success";

	}

	@RenderMapping(params = "action=searchBooking")
	public String handelSearchBookingRenderMapping(RenderRequest request,
			RenderResponse response) {

		return "searchBooking";
	}

	@RenderMapping(params = "action=deleteBooking")
	public String handelDeleteBookingRenderMapping(RenderRequest request,
			RenderResponse response) {

		return "deleteBooking";
	}

	@RenderMapping(params = "action=confirmation")
	public String handelConfirmationRenderMapping(RenderRequest request,
			RenderResponse response, ModelMap map) {
		return "confirmation";
	}

	@ActionMapping(params = "action=confirmation")
	public void update(ActionRequest request, ActionResponse response,
			ModelMap map) {

		String bid = ParamUtil.get(request, "bookingId", "default id");

		String bookName = ParamUtil.getString(request, "bookingName",
				"default Booking Name");
		String description = ParamUtil.getString(request, "bookingDescription",
				"default Booking Name");
		double price = ParamUtil.getDouble(request, "bookingAmt");
		String location = ParamUtil.getString(request, "bookingLocation",
				"default Booking location");

		_log.info("----------inside action---------------");
		response.setRenderParameter("bookingName", bookName);
		response.setRenderParameter("bookingDescription", description);
		response.setRenderParameter("bookingAmt", Double.toString(price));
		response.setRenderParameter("bookingLocation", location);

		response.setRenderParameter("action", "confirmation");

	}

	@ActionMapping(params = "action=saveBook")
	public void saveBook(ActionRequest request, ActionResponse response) {

		String bid = ParamUtil.get(request, "bookingId", "default id");

		String bookName = ParamUtil.getString(request, "bookingName",
				"default Booking Name");
		String description = ParamUtil.getString(request, "bookingDescription",
				"default Booking Name");
		double price = ParamUtil.getDouble(request, "bookingAmt");
		String location = ParamUtil.getString(request, "bookingLocation",
				"default Booking location");

		try {

			Booking booking = BookingLocalServiceUtil.createBooking(Long
					.valueOf((Calendar.getInstance().getTimeInMillis())));

			booking.setBookName(bookName);
			booking.setDescription(description);
			booking.setPrice(price);
			booking.setLocation(location);

			BookingLocalServiceUtil.addBooking(booking);

		} catch (Exception e) {
			_log.error("Exception while saving booking : ", e);
		}
		_log.info("----------inside action---------------");

	}

	@ActionMapping(params = "action=searchBooking")
	public void searchBook(ActionRequest request, ActionResponse response) {

		String bid = ParamUtil.get(request, "bookingId", "default id");

		String bookName = ParamUtil.getString(request, "bookingName",
				"default Booking Name");
		String description = ParamUtil.getString(request, "bookingDescription",
				"default Booking Name");
		double price = ParamUtil.getDouble(request, "bookingAmt");
		String location = ParamUtil.getString(request, "bookingLocation",
				"default Booking location");

		try {

			Booking booking = BookingLocalServiceUtil.createBooking(Long
					.valueOf((Calendar.getInstance().getTimeInMillis())));

			booking.setBookName(bookName);
			booking.setDescription(description);
			booking.setPrice(price);
			booking.setLocation(location);

			BookingLocalServiceUtil.addBooking(booking);

		} catch (Exception e) {
			_log.error("Exception while saving booking : ", e);
		}
		_log.info("----------inside action---------------");

	}

	@ActionMapping(params = "action=deleteBooking")
	public void deleteBook(ActionRequest request, ActionResponse response) {

		String bid = ParamUtil.get(request, "bookingId", "default id");

		String bookName = ParamUtil.getString(request, "bookingName",
				"default Booking Name");
		String description = ParamUtil.getString(request, "bookingDescription",
				"default Booking Name");
		double price = ParamUtil.getDouble(request, "bookingAmt");
		String location = ParamUtil.getString(request, "bookingLocation",
				"default Booking location");

		try {

			Booking booking = BookingLocalServiceUtil.createBooking(Long
					.valueOf((Calendar.getInstance().getTimeInMillis())));

			booking.setBookName(bookName);
			booking.setDescription(description);
			booking.setPrice(price);
			booking.setLocation(location);

			BookingLocalServiceUtil.addBooking(booking);

		} catch (Exception e) {
			_log.error("Exception while saving booking : ", e);
		}
		_log.info("----------inside action---------------");

	}

}
