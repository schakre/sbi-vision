<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ include file="../booking/init.jsp"%>


<portlet:renderURL var="defaultURL"/>

<p><a href="<%=defaultURL.toString()%>">Back</a></p>

<portlet:actionURL var="saveURL">
	<portlet:param name="action" value="saveBook"/>
</portlet:actionURL>


<portlet:renderURL var="saveBookingURL">
	<portlet:param name="action" value="addBooking"/>
	<portlet:param name="bookingName" value='<%=renderRequest.getParameter("bookingName")%>'/>
	<portlet:param name="bookingAmt" value='<%=renderRequest.getParameter("bookingAmt")%>'/>
	<portlet:param name="bookingLocation" value='<%=renderRequest.getParameter("bookingLocation")%>'/>
</portlet:renderURL>

<portlet:defineObjects />


<form action="<%=saveURL.toString()%>" method="post">

<table>

<tr><td> Booking Id   </td>    <td>  <input type="text" name="<portlet:namespace/>bookingId"  readonly="readonly"  /></td></tr>

<tr><td> Booking Name </td>    <td> <input type="text" name="<portlet:namespace/>bookingName" readonly="readonly" value="<%=renderRequest.getParameter("bookingName")%>" /></td></tr>

<tr><td> Booking Amount </td>   <td> <input type="text" name="<portlet:namespace/>bookingAmt" readonly="readonly" value="<%=renderRequest.getParameter("bookingAmt")%>"/></td></tr>

<tr><td> Booking Location </td> <td><input type="text" name="<portlet:namespace/>bookingLocation" readonly="readonly" value="<%=renderRequest.getParameter("bookingLocation")%>" /></td></tr>

          <tr ><td style="text-align:center"><input type="submit" value="Confirm"/></td>
          <td style="text-align:center"> <a href=<%=saveBookingURL.toString()%>>Cancel</a> </td></tr>
          
          
          </table>

</form>
