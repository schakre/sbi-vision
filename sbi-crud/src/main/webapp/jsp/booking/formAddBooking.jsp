<%@ include file="../booking/init.jsp"%>

<portlet:actionURL var="submitFormURL">
	<portlet:param name="action" value="formAddBooking"/>
</portlet:actionURL>


<form:form name="booking" method="post" modelAttribute="booking" action="<%=submitFormURL.toString() %>">
<br/>
    <table style="margin-left:80px">
        <tbody>
            <tr>
                <td><form:label path="bookName">bookName</form:label></td>
                <td><form:input path="bookName"></form:input></td>
            </tr>
            <tr>
                <td><form:label path="description">description</form:label></td>
                <td><form:input path="description"></form:input></td>
            </tr>
            <tr>
                <td><form:label path="price">price</form:label></td>
                <td><form:input path="price"></form:input></td>
            </tr>
            <tr>
                <td><form:label path="location">location</form:label></td>
                <td><form:input path="location"></form:input></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Submit Form">
                </td>
            </tr>
        </tbody>
    </table>
</form:form>